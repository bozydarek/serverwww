# Autor: Piotr Szymajda - 273 023
CXX = g++
FLAGS = -std=c++11 -Wall -Wextra -pedantic -pedantic-errors# -O2
NAME = serverWWW
OBJS = main.o socket_op.o server.o

all: $(OBJS) 
	$(CXX) $^ -o $(NAME)

$(OBJS): %.o: %.cpp
	$(CXX) -c $(FLAGS) $< -o $@

debug: FLAGS += -DDEBUG -g3
debug: all

release: FLAGS += -O2
release: all

clean:
	rm -f *.o
	
distclean: clean
	rm -f $(NAME)
	
.PHONY: all clean distclean debug
