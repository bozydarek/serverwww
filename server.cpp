#include "server.h"

#include <iostream>
#include <assert.h>
#include <sys/time.h>
#include <cstddef>
#include <limits.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <fstream>

#include "socket_op.h"

#define BACKLOG 32

#define WAIT_TIME 30000000LL // 10 sec in microsec

#define microsec_to_sec 1000000LL
#define microsec_to_milisec 1000LL

using namespace std;

struct buffer
{
	int 	len;		// how much data is in buffer
	int 	cnt;		// how much data was read from buffer
	char 	buff[4096];
};

struct get_query
{
	bool 	get;			// true = GET, false = not GET (for examle POST or HEAD)
	bool 	connection; 	// true = keep-alive, false = close
	string 	host;
	string	what;
};

int ReadBufferedByte (int fd, buffer& buff, char* c, struct timeval* tv)
{
	if( buff.cnt == buff.len ) 
	{
		#ifdef DEBUG 
			cout << "Waiting...\n";
		#endif
		
		buff.cnt = 0;
		
		fd_set 	descriptors;
		FD_ZERO (&descriptors);
		FD_SET 	(fd, &descriptors);
		
		if( Select (fd+1, &descriptors, tv) == 0 )
		{
			return -1;
		}
		
		#ifdef DEBUG
			cout << "Current value of tv = "<< tv->tv_sec + tv->tv_usec * 1.0 / 1000000 << "\n";
		#endif
		
		buff.len = Recv (fd, buff.buff, 4096, 0);
		if( buff.len <= 0 )
		{
			return buff.len;
		}
	}
	
	*c = buff.buff[buff.cnt++];
	
	return 1;
}

int ReadLine (int fd, buffer& buff, char* rec_buff, int max_len, int timeout) 
{	
	int 	n = 0;
	char 	c;
	int 	cnt;
	struct timeval tv{0, timeout}; 
	
	while( (cnt = ReadBufferedByte (fd, buff, &c, &tv)) ) 
	{
		if( tv.tv_sec == 0 && tv.tv_usec == 0 ) 
		{ 
			return -2; 
		}
		if( cnt == -1 ) 
		{
			return -1; 
		}
		if( n < max_len ) 
		{ 
			rec_buff[n++] = c;
		}
		//cout << c ;
		//cout << "-" << (int)c << "\n"; 
		if( c == '\n' ) 
		{ 
			break;
		}
	}
	return n;
}

string prepare_msg(string code, string type, string content, string location = "")
{
	if( location != "" )
	{
		return "HTTP/1.1 " + code + "\nLocation: " + location + "\nContent-length: " + to_string(content.length()+1) + "\nContent-Type: " + type + "\n\n" + content + "\0";
	}
	return "HTTP/1.1 " + code + "\nContent-length: " + to_string(content.length()) + "\nContent-Type: " + type + "\n\n" + content + "\0";
}

string resolve_type( string path )
{
	size_t f = path.rfind('.');
	
	if( f != string::npos )
	{
		string type = path.substr (f+1);
		
		#ifdef DEBUG
			cout << "\033[34mFILE TYPE: #\033[0m" << type << "\033[34m#\033[0m\n";
		#endif
	
		if( type == "txt" )
		{
			return "text/plain";
		}
		
		string text [] = {"html", "css"};
		string images[] = {"jpg", "jpeg", "png"};
	
		for( auto ext : text )
		{
			if(ext == type)
				return "text/" + type;
		}
			
		for( auto ext : images )
		{
			if(ext == type)
				return "image/" + type;
		}
		
		if( type == "pdf" )
		{
			return "application/pdf";
		}
	}
	return "application/octet-stream";
}

int server (unsigned int port, char* path)
{
	int sockfd = Socket (AF_INET,  SOCK_STREAM, 0);
	
	struct sockaddr_in server_address;
	bzero (&server_address, sizeof(server_address));
	server_address.sin_family		= AF_INET;
	server_address.sin_port			= htons (port);
	server_address.sin_addr.s_addr	= htonl (INADDR_ANY);
	
	Bind (sockfd, (struct sockaddr*)&server_address, sizeof(server_address));

 	Listen( sockfd, BACKLOG );

	
	struct sockaddr_in	sender;	
	const int 			max_size = 4096;
	int 				conn_sockfd;
	
	cout <<  "\033[1mServer started on port: " << port << "\033[0m\n";
	
	while( true )
	{
	    if( ( conn_sockfd = Accept( sockfd, ( struct sockaddr * ) & sender ) ) == - 1 ) 
	    {
	        continue;
	    }
	    
	    //#ifdef DEBUG
		    cout << "\033[1mConnection from \033[0m" << inet_ntoa( sender.sin_addr ) << '\n';
		//#endif
		
		get_query tmp;
		do
		{
			#ifdef DEBUG
				cout << "Reciving\n";
			#endif
				
			buffer buff{0,0,"\0"};
			char recv_buffer[max_size+1];
			int n;
			
			bool whatever = false;
			
			tmp = {false, false, "", ""};
			
			while ( (n = ReadLine (conn_sockfd, buff, recv_buffer, max_size, 10000)) ) 
			{
				if( n < 0) 
				{				
					#ifdef DEBUG
						cout << "Readline error: " << (n == -1 ? strerror(errno) : "timeout") << "\n";
					#endif
					break;	
				}
				recv_buffer[n] = '\0';
			
				#ifdef DEBUG
					cout << "\033[33mRECV: #\033[0m" << recv_buffer << "\033[33m#\033[0m\n";
				#endif
				whatever = true;

				if( strncmp(recv_buffer, "GET", 3) == 0 ) 
				{
					tmp.get = true;
					tmp.what = recv_buffer + 4;
					
					size_t f = tmp.what.find(' ');
					if( f != string::npos )
					{
						tmp.what.resize (f);
						cout << "\033[34mWHAT: #\033[0m" << tmp.what << "\033[34m#\033[0m\n";
					}
				}
				else if( strncmp(recv_buffer, "Host: ", 6) == 0 ) 
				{
					tmp.host = recv_buffer + 6;
					tmp.host.resize(tmp.host.length()-2);
				
					cout << "\033[34mHOST: #\033[0m" << tmp.host << "\033[34m#\033[0m\n";
				}
				else if( strncmp(recv_buffer, "Connection: ", 12) == 0 ) 
				{
					string tt (recv_buffer + 12);
					tt.resize(tt.length()-2);
					
					if( tt == "keep-alive" )
					{
						tmp.connection = true;
						
						cout << "\033[34mKEEP: #\033[0m" << tmp.connection << "\033[34m#\033[0m\n";
					}
				}
				
			}
			
			if( !whatever )
			{
				break;
			}
			
			#ifdef DEBUG
					cout << "Sending\n";
			#endif
				
			if( tmp.get )
			{
				size_t f = tmp.host.rfind(':');
				string host;
				if( f != string::npos )
				{
					host = tmp.host.substr(0,f);
					//cout << "\033[34mHOST: #\033[0m" << tmp.host << "\033[34m#\033[0m\n";
				}
				else
				{
					cout << "\033[1;31mError, unknown port\033[0m\n";
				}
			
				string tmp_path(path);
				tmp_path += '/' + host + tmp.what;
			
				#ifdef DEBUG
					cout << "Path: " << tmp_path << '\n';
				#endif
	
				string text;
			
				char resolved_path[PATH_MAX];
				if ( realpath(tmp_path.c_str(), resolved_path) == NULL )
				{
					cout << "\033[31mError, can't find: #\033[0m" << tmp_path << "\033[31m#\033[0m\n";
					text = prepare_msg("404 Not Found","text/html","<html><title>Ops! - 404 - Not Found</title><head><H1>Ops! - 404 - Not Found</H1></head><body><H3>Sorry! We can't seem to find what you're looking for ;(</H3></body></html>");
				}
				else if( strncmp(resolved_path, path, strlen(path)) != 0 )
				{
					 text = prepare_msg("403 Forbidden","text/html","<html><title>Ops! - 403 - Forbidden</title><head><H1>Ops! - 403 - Forbidden</H1></head><body><H3>Sorry, but you are not allowed to be here!</H3></body></html>");
				}
				else
				{
					struct stat info;

					if( stat( resolved_path, &info ) != 0 )
					{
						text = prepare_msg("404 Not Found","text/html","<html><title>Ops! - 404 - Not Found</title><head><H1>Ops! - 404 - Not Found</H1></head><body><H3>Sorry! We can't seem to find what you're looking for ;(</H3></body></html>");
					}
					else if( info.st_mode & S_IFDIR )
					{
						string addr;
						if( tmp.what != "/" )
						{
							addr = "http://" + tmp.host + tmp.what + "/index.html";
						}
						else
						{
							assert( tmp.what == "/" );
							addr = "http://" + tmp.host + tmp.what + "index.html";
						}
						
						text = prepare_msg( "301 Moved Permanently", "text/html", "<html><title>301 - Moved</title><head><H1>301 - Moved</H1></head><body><h1>Moved</h1><p>This page has moved to <a href=\"" + addr + "\">" + addr + "</a>.</p></body></html>", addr );

					}
					else
					{
						ifstream ifs( resolved_path, ifstream::in | ifstream::binary );
						string content( (istreambuf_iterator<char>(ifs) ), (istreambuf_iterator<char>()) );
						text = prepare_msg( "200 OK", resolve_type( resolved_path ), content );
					}
				}
				Send (conn_sockfd, (char*)text.c_str(), text.length(), 0);
			
			}
			else
			{
				string text = prepare_msg("501 Not Implemented","text/html","<html><title>Ops! - 501 - Not Implemented</title><head><H1>Ops! - 501 - Not Implemented</H1></head><body><H3>Sorry! We still working on that.</H3></body></html>");
		
				Send (conn_sockfd, (char*)text.c_str(), text.length(), 0);
			}

		
		}while(	tmp.connection );
		
		Close (conn_sockfd);
		//cout <<  "\033[1mDisconnected \033[0m\n";
	}
	
	Close (sockfd);
	
	return 0;
}

