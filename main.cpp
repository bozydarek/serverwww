/*
Autor: Piotr Szymajda - 273 023

Server WWW
*/
#include <iostream>
#include <cstring>
#include <assert.h>
#include <sstream>
#include <algorithm>

#include <limits.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>


#include "server.h"
#include "socket_op.h"

using namespace std;

void man ();
void get_path (char* buff, int len);

int main (int argc, char *argv[])
{
    if (argc != 3)
    {   
        if( argc == 2 and (strcmp (argv [1], "--help") == 0 or strcmp (argv [1], "-h") == 0 ) )
        {
            man ();
        }
        else
        {
            cout << "\033[1;31mError:\033[0m Incorrect use\n";
            man ();
        }
        return 1;
    }

	unsigned int port;
	{
		istringstream ss( argv[1] );
		if( not (ss >> port) )
		{
			cout << "\033[1;31mError:\033[0m " << argv[1] << " is not a number\n";
			return 2;
		}
		if( not (port > 0) )
		{
			cout << "\033[1;31mError:\033[0m Port must be greater then 0 \n";
			return 3;
		}
	}
	
	// Path check:
	
	char path[PATH_MAX];
	if( argv[2][0] == '/' or (argv[2][0] == '~' and argv[2][1] == '/') )
	{
		strcpy(path, argv[2]);
	}
	else
	{
		get_path(path, PATH_MAX);
		strcat(path, argv[2]);
	}
	
	#ifdef DEBUG
		cout << "Path: " << path << '\n';
	#endif
	
	char resolved_path[PATH_MAX];
	if ( realpath(path, resolved_path) == NULL )
	{
		print_error("realpath", EXIT_FAILURE);
	}
	
	//Based on: http://stackoverflow.com/a/18101042
	{
		struct stat info;

		if( stat( resolved_path, &info ) != 0 )
		{
			cout << "\033[1;31mError:\033[0;31m Cannot access \033[0m" << resolved_path << '\n';
			return 4;
		}
		else if( info.st_mode & S_IFDIR )
		{
			#ifdef DEBUG		
				cout << "\033[1;32mOK:\033[0m " << resolved_path << "\033[32m is directory\033[0m\n";
			#endif
		}
		else
		{
			cout << "\033[1;31mError:\033[0m " << resolved_path << "\033[31m is no directory\033[0m\n";
			return 5;
		}
	}
	
	cout << "\033[1mPath: \033[0m" << resolved_path << '\n';
	
	// Start server on port 
	return server (port, resolved_path);
    
    return 0;
}

/*
Function displays manual
*/
void man()
{
    cout << "\033[1;36mserverWWW\033[0m - simple server implementation\n";
    cout << "\n\033[1mUsage:\033[0m\n";
    cout << "\t./serverWWW port dest - for start program\n";
    cout << "\t./serverWWW -h - for display this help\n";
    cout << "\n";
    cout << "port - number of port for server to start listen\n";
    cout << "dest - directory path to folder with websides\n";
    cout << "\n\033[1mFor example:\033[0m\n";
    cout << "\t./serverWWW 80 ./strony_www\n";
}


// Based on: http://stackoverflow.com/a/198099
void get_path (char* buff, int len)
{
	char tmp[32];
	sprintf (tmp, "/proc/%d/exe", getpid());
	
	int r = readlink(tmp, buff, len);

	if (r == -1) 
	{
		print_error("readlink", EXIT_FAILURE);
	}

	
	int bytes = min( r, len - 1 );
	
	char * p = strrchr (buff,'/');	
		
	if( bytes >= 0 )
	{
		buff[p-buff+1] = '\0';
	}
}
